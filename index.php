<?php
  require 'models/connectionDB.php';
  require_once 'models/categorie.php';
  require_once 'models/article.php';

  $categorySelected = "";
?>

<!DOCTYPE html>
<html lang="fr">
  <?php
    include 'views/partials/header.php';
  ?>

  <body>
    <?php
      include 'views/partials/nav.php';
    ?>

    <div id="wrapper" class="d-grid min-vh-100" style="grid-template-rows: auto 1fr auto;">

      <div class="container mx-auto py-8">
        <div class="row row-cols-1 row-cols-md-3 g-4">
          <main class="col-xl-12">
            <h2 class="text-lg fw-semibold mb-4 bg-white rounded-lg text-danger p-2 text-center">Actualités <?php if (isset($_GET['categoryName'])) {echo $_GET['categoryName']; } ?> </h2>

            <div class="row row-cols-1 mr-1 row-cols-md-2 g-4" id="news-container">
              <?php
                // Récuperer les articles en fonction de la catégorie
                $categoryFilter = isset($_GET['category']) ? $_GET['category'] : 'all';
                $sql = "SELECT * FROM Article";
                if ($categoryFilter !== 'all') {
                  $sql .= " WHERE categorie = " . $categoryFilter;
                }

                // Récupérer les articles de la categorie correspondant
                $result = pg_query($conn, $sql);

                if (pg_num_rows($result) > 0) {
                  while ($row = pg_fetch_assoc($result)) {
                    $article = new article($row['id'], $row['titre'], $row['contenu'], $row['categorie']);

                    echo '<div class="bg-white p-4 card">';
                    echo '<h3 class="text-lg fw-semibold">'. $article->getTitre() .'</h3>';
                    echo '<p class="text-truncate">'. $article->getContenu() .'</p>';
                    echo '<a href="views/detailArticle.php?id='. $article->getId() .'"> En savoir plus </a>';
                    echo '</div>';
                  }
                } else {
                  echo "<p class='text-warning'>Aucun article trouvé.</p>";
                }
              ?>
            </div>
          </main>
        </div>
      </div>    
    </div>

    <?php
      include 'views/partials/footer.php';
    ?>
  </body>

</html>



 
