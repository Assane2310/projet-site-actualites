<?php 
    $host = "localhost"; // Adresse du serveur PostgreSQL
    $port = "5432"; // Port du serveur PostgreSQL
    $dbname = "mglsi_news"; // Nom de la base de données
    $user = "postgres"; // Nom d'utilisateur PostgreSQL
    $password = "passer123"; // Mot de passe PostgreSQL

    // Connexion à la base de données
    $conn = pg_connect("host=$host dbname=$dbname user=$user password=$password");

    // Vérifier la connexion
    if (!$conn) {
        die("Échec de la connexion : " . pg_last_error());
    }
?> 
