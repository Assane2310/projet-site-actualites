<?php
    require '../models/connectionDB.php';
    require_once '../models/categorie.php';

    // Afficher toutes les catégories
    function afficherCategories($conn) {
        $sql = "SELECT * FROM categorie";
        $result = pg_query($conn, $sql);

        if (!$result) {
            echo "Erreur de requête";
            return;
        }

        if (pg_num_rows($result) > 0) {
            echo '<table class="table table-bordered">';
            echo '<thead class="thead-dark">';
            echo '<tr><th>ID</th><th>Libelle</th><th>Action</th></tr>';
            echo '</thead>';
            echo '<tbody>';
            while ($row = pg_fetch_assoc($result)) {
                $category = new categorie($row['id'], $row['libelle']);

                echo '<tr>';
                echo '<td>' . $category->getId() . '</td>';
                echo '<td>' . $category->getLibelle() . '</td>';
                echo '<td><a href="crudCategorie.php?action=edit&id=' . $category->getId() . '" class="btn btn-primary">Modifier</a> <a href="crudCategorie.php?action=delete&id=' . $category->getId() . '" class="btn btn-danger">Supprimer</a></td>';
                echo '</tr>';
            }
            echo '</tbody>';
            echo '</table>';
        } else {
            echo "Aucune catégorie trouvée.";
        }
    }

    // Ajouter ou modifier une catégorie
    if (isset($_POST['submit'])) {
        $libelle = $_POST['libelle'];

        if (isset($_GET['action']) && $_GET['action'] === 'edit') {
            $id = $_GET['id'];
            $sql = "UPDATE categorie SET libelle = '$libelle' WHERE id = '$id'";
        } else {
            $sql = "INSERT INTO categorie (libelle) VALUES ('$libelle')";
        }

        $result = pg_query($conn, $sql);
        if (!$result) {
            echo "Erreur lors de l'ajout ou de la modification de la catégorie.";
        }
    }

    // Supprimer une catégorie
    if (isset($_GET['action']) && $_GET['action'] === 'delete' && isset($_GET['id'])) {
        $id = $_GET['id'];
        $sql = "DELETE FROM categorie WHERE id = '$id'";
        $result = pg_query($conn, $sql);
        if (!$result) {
            echo "Erreur lors de la suppression de la catégorie.";
        }
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrateur</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>    
</head>
<body>
    <?php
        require 'partials/navAdmin.php';
    ?>  

    <div class="container mt-4">
        <?php afficherCategories($conn); ?>
    </div>

    <div class="container">
        <h2 class="mt-5">Ajouter/Modifier une catégorie</h2>
        <form method="post">
            <div class="form-group">
                <label for="libelle">Libellé :</label>
                <input type="text" class="form-control" name="libelle" id="libelle" required>
            </div>
            <button type="submit" name="submit" class="btn btn-primary">Enregistrer</button>
        </form>
    </div>
</body>
</html>
