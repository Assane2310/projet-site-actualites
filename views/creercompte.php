<?php
    // Vérifiez si le formulaire a été soumis
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $prenom = $_POST['prenom'];
        $nom = $_POST['nom'];
        $login = $_POST['login'];
        $password = $_POST['password'];

        require '../models/connectionDB.php';

        // Vérifier si un utilisateur existe déjà avec le même login
        $query = "SELECT * FROM Utilisateur WHERE login = '$login'";
        $result = pg_query($conn, $query);

        if (!$result) {
            echo "Erreur de requête";
            exit;
        }

        if (pg_num_rows($result) > 0) {
            $errorMessage = "Un utilisateur avec ce login existe déjà. Veuillez choisir un autre login.";
        } else {
            // Hacher le mot de passe saisi
            $passwordHasher = password_hash($password, PASSWORD_BCRYPT);

            // Requête pour insérer le nouvel utilisateur dans la base de données
            $queryInsert = "INSERT INTO Utilisateur (prenom, nom, login, password) VALUES ('$prenom', '$nom', '$login', '$passwordHasher')";
            $resultInsert = pg_query($conn, $queryInsert);

            if (!$resultInsert) {
                echo "Erreur lors de la création du compte";
                exit;
            }

            header("Location: authentification.php");
            exit;
        }

        pg_close($conn);
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Créer un compte</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>    
    <link rel="stylesheet" href="../public/css/style.scss">
</head>
<body>
    <div class="container">
            <div class="row">
                <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div class="card border-0 shadow rounded-3 my-5">
                        <div class="card-body p-4 p-sm-5">
                            <h5 class="card-title text-center mb-5 fw-light fs-5">Créer un compte</h5>

                            <?php if (isset($errorMessage)) : ?>
                                <p class="text-danger"><?php echo $errorMessage; ?></p>
                            <?php endif; ?>

                            <form method="post">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="prenom" id="prenom" required>
                                    <label for="prenom">Prénom :</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="nom" id="nom" required>
                                    <label for="nom">Nom :</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="login" id="login" required>
                                    <label for="login">Login :</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="password" class="form-control" name="password" id="password" required>
                                    <label for="password">Mot de passe :</label>
                                </div>

                                <div class="d-grid">
                                    <button class="btn btn-danger btn-login text-uppercase fw-bold" type="submit">Enregistrer</button>
                                </div>
                                <hr class="my-4">

                                <div class="d-grid">
                                    <a class="text-danger text-center" href="authentification.php">Page de connexion</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html>
