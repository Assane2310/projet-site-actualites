<?php
    require '../models/connectionDB.php';

    require_once '../models/article.php';


    // Afficher tous les articles
    function afficherArticles($conn) {
        $sql = "SELECT * FROM article";
        $result = pg_query($conn, $sql);

        if (!$result) {
            echo "Erreur de requête";
            return;
        }

        if (pg_num_rows($result) > 0) {
            echo '<table class="table table-bordered">';
            echo '<thead class="thead-dark">';
            echo '<tr><th>ID</th><th>Titre</th><th>Contenu</th><th>Catégorie</th><th>Action</th></tr>';
            echo '</thead>';
            echo '<tbody>';
            while ($row = pg_fetch_assoc($result)) {
                $article = new Article($row['id'], $row['titre'], $row['contenu'], $row['categorie']);
                echo '<tr>';
                echo '<td>' . $article->getId() . '</td>';
                echo '<td>' . $article->getTitre() . '</td>';
                echo '<td>' . $article->getContenu() . '</td>';
                echo '<td>' . $article->getCategorie() . '</td>';
                echo '<td><a href="crudArticle.php?action=edit&id=' . $article->getId() . '" class="btn btn-primary">Modifier</a> <a href="crudArticle.php?action=delete&id=' . $article->getId() . '" class="btn btn-danger">Supprimer</a></td>';
                echo '</tr>';
            }
            echo '</tbody>';
            echo '</table>';
        } else {
            echo "Aucun article trouvé.";
        }
    }

    // Ajouter ou modifier un article
    if (isset($_POST['submit'])) {
        $titre = $_POST['titre'];
        $contenu = $_POST['contenu'];
        $categorie = $_POST['categorie'];

        if (isset($_GET['action']) && $_GET['action'] === 'edit') {
            $id = $_GET['id'];
            $sql = "UPDATE article SET titre = '$titre', contenu = '$contenu', categorie = '$categorie' WHERE id = '$id'";
        } else {
            $sql = "INSERT INTO article (titre, contenu, categorie) VALUES ('$titre', '$contenu', '$categorie')";
        }

        $result = pg_query($conn, $sql);
        if (!$result) {
            echo "Erreur lors de l'ajout ou de la modification de l'article.";
        }
    }

    // Supprimer un article
    if (isset($_GET['action']) && $_GET['action'] === 'delete' && isset($_GET['id'])) {
        $id = $_GET['id'];
        $sql = "DELETE FROM article WHERE id = '$id'";
        $result = pg_query($conn, $sql);
        if (!$result) {
            echo "Erreur lors de la suppression de l'article.";
        }
    }
?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrateur</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>    
</head>
<body>
    <?php
        require 'partials/navAdmin.php';
    ?>  

    <div class="container mt-4">
        <?php afficherArticles($conn); ?>
    </div>

    <div class="container mt-4">
        <h2>Ajouter/Modifier un article</h2>
        <form method="post">
            <div class="form-group">
                <label for="titre">Titre :</label>
                <input type="text" class="form-control" name="titre" id="titre" required>
            </div>
            <div class="form-group">
                <label for="contenu">Contenu :</label>
                <textarea class="form-control" name="contenu" id="contenu" rows="5" required></textarea>
            </div>
            <div class="form-group">
                <label for="categorie">Catégorie :</label>
                <input type="text" class="form-control" name="categorie" id="categorie" required>
            </div>
            <button type="submit" class="btn btn-primary" name="submit">Enregistrer</button>
        </form>
    </div>
</body>
</html>







