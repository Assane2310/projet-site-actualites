<?php
    require '../models/connectionDB.php';
    require_once '../models/categorie.php';
    require_once '../models/article.php';

    $categorySelected = "";

    // Récupérez l'identifiant de l'article depuis la requête GET
    $articleId = $_GET['id'];
?>

<!DOCTYPE html>
<html lang="fr">
    <?php
        include 'partials/header.php';
    ?>

    <body>
        <?php
            include 'partials/nav.php';

            $query = "SELECT * FROM article WHERE id = '$articleId'";
            $result = pg_query($conn, $query);

            if (!$result) {
                echo "Erreur de requête";
                exit;
            }

            if (pg_num_rows($result) > 0) {
                $row = pg_fetch_assoc($result);

                $article = new article($row['id'], $row['titre'], $row['contenu'], $row['categorie']);

                echo "<div class='d-flex justify-content-center'>";
                    echo "<div class='card p-5'>";
                        echo "<h2 class='card-header'>" . $article->getTitre() . "</h2>";
                        echo "<div class='card'>";
                            echo "<div class='card-body p-3'>" . $article->getContenu() . "</div>";
                        echo "</div>";    
                    echo "</div>";
                echo "</div>";
            } else {
                echo "Article non trouvé";
            }

            include 'partials/footer.php';
        ?>
    </body>

</html>