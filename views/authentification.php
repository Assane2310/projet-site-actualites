<?php
    require '../models/connectionDB.php';

    session_start();

    // Vérifiez si le formulaire a été soumis
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $loginSaisi = $_POST['login'];
        $passwordSaisi = $_POST['password'];

        // Récupérer les informations de l'utilisateur à partir du login saisi
        $query = "SELECT * FROM Utilisateur WHERE login = '$loginSaisi'";
        $result = pg_query($conn, $query);

        if (!$result) {
            echo "Erreur de requête";
            exit;
        }

        // Vérifier l'utilisateur
        if (pg_num_rows($result) > 0) {
            $user = pg_fetch_assoc($result);
            $passwordVrai = $user['password'];

            // Comparer le mot de passe saisi et le mot de passe haché
            if (password_verify($passwordSaisi, $passwordVrai)) {
                $_SESSION['isAuthenticated'] = true;

                $_SESSION['userId'] = $user['id'];

                header("Location: admin.php");
                exit;
            } else {
                $errorMessage = "Identifiant ou mot de passe incorrect.";
            }
        } else {
            $errorMessage = "Identifiant ou mot de passe incorrect.";
        }
    }
?>

<!doctype html>
<html lang="en">
    <head>
        <title>Authentification</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        
        <link rel="stylesheet" href="../public/css/style.scss">
	</head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div class="card border-0 shadow rounded-3 my-5">
                        <div class="card-body p-4 p-sm-5">
                            <h5 class="card-title text-center mb-5 fw-light fs-5">Sign In</h5>

                            <?php if (isset($errorMessage)) : ?>
                                <p class="text-danger"><?php echo $errorMessage; ?></p>
                            <?php endif; ?>

                            <form method="post">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="login" id="login" placeholder="name@example.com">
                                    <label for="login">Login</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                    <label for="password">Password</label>
                                </div>
                                <div class="d-grid">
                                    <button class="btn btn-danger btn-login text-uppercase fw-bold" type="submit">Se connecter</button>
                                </div>
                                <hr class="my-4">
                                <div class="d-grid">
                                    <button class="btn btn-danger btn-login text-uppercase fw-bold" type="button"><a class="text-white" href="creercompte.php">Créer un compte</a></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

