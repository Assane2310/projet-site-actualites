<?php
    session_start();

    if (isset($_SESSION['isAuthenticated']) && $_SESSION['isAuthenticated'] === true) {
        // Récupérer l'identifiant de l'utilisateur authentifié (à partir de la session)
        $userId = $_SESSION['userId'];

        // Récupérer les informations spécifiques de l'utilisateur
        $query = "SELECT * FROM utilisateur WHERE id = $userId";
        $result = pg_query($conn, $query);
    } else {
        // Redirigez l'utilisateur vers la page de connexion (ou une autre page appropriée)
        header("Location: authentification.php");
        exit;
    }
?>
<nav class="navbar navbar-expand-lg navbar-dark p-3 bg-danger" id="headerNav">
        <div class="container-fluid">
            <?php
                // Vérifier si l'utilisateur existe
                if (pg_num_rows($result) > 0) {
                    $user = pg_fetch_assoc($result);
                    
                    echo "<h1 class='text-white'>{$user['prenom']} {$user['nom']}</h1>";

                } else {
                    echo "Informations utilisateur non trouvées";
                }
            ?>
        </div>

        <?php
            echo '<form action="deconnecter.php" method="post">';
            echo '<button type="submit" class="btn btn-danger">Se déconnecter</button>';
            echo '</form>';
        ?>
    </nav>