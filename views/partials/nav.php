<nav class="navbar navbar-expand-lg navbar-dark p-3 bg-danger" id="headerNav">
	<div class="container-fluid">
      <a class="navbar-brand d-block" href="http://localhost/Assane/index.php?category=all">
          <img src="http://localhost/Assane/assets/img/logotype_ESP_UCAD.png" height="60" />
          <strong class="ml-2">Actualités de l'Ouest</strong>
      </a>

	    
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    
	    <div class=" collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mx-auto ">
	            <li class="nav-item">
	              <a class="nav-link mx-2 active" aria-current="page" href="http://localhost/Assane/index.php?category=all">Home</a>
	            </li>
	            <?php
                // Récuperer toutes les catégories
                $sql = "SELECT * FROM Categorie";
                $result = pg_query($conn, $sql);

                if (pg_num_rows($result) > 0) {
                  while ($row = pg_fetch_assoc($result)) {
                    $category = new categorie($row['id'], $row['libelle']);

                    echo '<li class="nav-item hover"><a href="http://localhost/Assane/index.php?category=' . $category->getId() . '&amp;categoryName=' . urlencode($category->getLibelle()) . '" class="nav-link mx-2">' . $category->getLibelle() . '</a></li>';
                  }
                }
              ?>
	        </ul>
	        
	    </div>

      
      <button type="button" class="btn btn-danger border border-white"><a href="http://localhost/Assane/views/authentification.php" class="text-white">Authentification</a></button>
	</div>
</nav>