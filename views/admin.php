<?php
    session_start();

    if (isset($_SESSION['isAuthenticated']) && $_SESSION['isAuthenticated'] === true) {
        // Récupérer l'identifiant de l'utilisateur authentifié (à partir de la session)
        $userId = $_SESSION['userId'];

        require '../models/connectionDB.php';
        require_once '../models/categorie.php';
        require_once '../models/article.php';

        // Récupérer les informations spécifiques de l'utilisateur
        $query = "SELECT * FROM utilisateur WHERE id = $userId";
        $result = pg_query($conn, $query);
    } else {
        header("Location: authentification.php");
        exit;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrateur</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark p-3 bg-danger" id="headerNav">
        <div class="container-fluid">
            <?php
                // Vérifier si l'utilisateur existe
                if (pg_num_rows($result) > 0) {
                    $user = pg_fetch_assoc($result);

                    // Afficher les informations spécifiques de l'utilisateur
                    echo "<h1 class='text-white'>{$user['prenom']} {$user['nom']}</h1>";

                } else {
                    echo "Informations utilisateur non trouvées";
                }
            ?>
        </div>

        <?php
            echo '<form action="deconnecter.php" method="post">';
            echo '<button type="submit" class="btn btn-danger">Se déconnecter</button>';
            echo '</form>';
        ?>
    </nav>

    <div class="text-center mt-2">
        <a href="crudCategorie.php" class="btn btn-danger">Ajouter catégorie</a>
        <a href="crudArticle.php" class="btn btn-danger">Ajouter article</a>
    </div>  

    <div class="container mt-4">
        <h1>Catégories</h1>
        <ul>
            <?php
            // Récupérer toutes les catégories
            $sqlCategories = "SELECT * FROM Categorie";
            $resultCategories = pg_query($conn, $sqlCategories);

            if (!$resultCategories) {
                echo "Erreur de requête";
                exit;
            }

            // Afficher toutes les catégories
            if (pg_num_rows($resultCategories) > 0) {
                while ($row = pg_fetch_assoc($resultCategories)) {
                    $category = new categorie($row['id'], $row['libelle']);

                    echo '<li>' . $category->getLibelle() . '</li>';
                }
            } else {
                echo "Aucune catégorie trouvée.";
            }
            ?>
        </ul>

        <h1>Articles</h1>
        <ul>
            <?php
            // Récupérer tous les articles
            $sqlArticles = "SELECT * FROM article";
            $resultArticles = pg_query($conn, $sqlArticles);

            if (!$resultArticles) {
                echo "Erreur de requête";
                exit;
            }

            // Afficher toutess les articles
            if (pg_num_rows($resultArticles) > 0) {
                while ($row = pg_fetch_assoc($resultArticles)) {
                    $article = new article($row['id'], $row['titre'], $row['contenu'], $row['categorie']);

                    echo '<li>' . $article->getTitre() . '</li>';
                }
            } else {
                echo "Aucun article trouvé.";
            }
            ?>
        </ul>
    </div>
</body>
</html>
